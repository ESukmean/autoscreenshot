﻿namespace AutoScreenShot3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.btn_del = new System.Windows.Forms.Button();
			this.btn_clb_reverse = new System.Windows.Forms.Button();
			this.clb_filelist = new System.Windows.Forms.CheckedListBox();
			this.btn_open = new System.Windows.Forms.Button();
			this.btn_next = new System.Windows.Forms.Button();
			this.btn_past = new System.Windows.Forms.Button();
			this.Picturebox = new System.Windows.Forms.PictureBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.btn_stop = new System.Windows.Forms.Button();
			this.btn_start = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.btn_explorer = new System.Windows.Forms.Button();
			this.tb_path = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tableLayoutPanel1.SuspendLayout();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Picturebox)).BeginInit();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Controls.Add(this.splitContainer1, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(584, 462);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(3, 123);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.btn_open);
			this.splitContainer1.Panel2.Controls.Add(this.btn_next);
			this.splitContainer1.Panel2.Controls.Add(this.btn_past);
			this.splitContainer1.Panel2.Controls.Add(this.Picturebox);
			this.splitContainer1.Size = new System.Drawing.Size(578, 336);
			this.splitContainer1.SplitterDistance = 151;
			this.splitContainer1.TabIndex = 1;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.btn_del);
			this.groupBox2.Controls.Add(this.btn_clb_reverse);
			this.groupBox2.Controls.Add(this.clb_filelist);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox2.Location = new System.Drawing.Point(0, 0);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(151, 336);
			this.groupBox2.TabIndex = 0;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "파일 목록";
			// 
			// btn_del
			// 
			this.btn_del.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_del.Location = new System.Drawing.Point(77, 308);
			this.btn_del.Name = "btn_del";
			this.btn_del.Size = new System.Drawing.Size(70, 23);
			this.btn_del.TabIndex = 2;
			this.btn_del.Text = "삭제";
			this.btn_del.UseVisualStyleBackColor = true;
			// 
			// btn_clb_reverse
			// 
			this.btn_clb_reverse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btn_clb_reverse.Location = new System.Drawing.Point(3, 308);
			this.btn_clb_reverse.Name = "btn_clb_reverse";
			this.btn_clb_reverse.Size = new System.Drawing.Size(70, 23);
			this.btn_clb_reverse.TabIndex = 2;
			this.btn_clb_reverse.Text = "선택 반전";
			this.btn_clb_reverse.UseVisualStyleBackColor = true;
			// 
			// clb_filelist
			// 
			this.clb_filelist.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.clb_filelist.FormattingEnabled = true;
			this.clb_filelist.Location = new System.Drawing.Point(3, 15);
			this.clb_filelist.Name = "clb_filelist";
			this.clb_filelist.Size = new System.Drawing.Size(145, 276);
			this.clb_filelist.TabIndex = 1;
			// 
			// btn_open
			// 
			this.btn_open.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_open.Location = new System.Drawing.Point(155, 295);
			this.btn_open.Name = "btn_open";
			this.btn_open.Size = new System.Drawing.Size(110, 39);
			this.btn_open.TabIndex = 2;
			this.btn_open.Text = "보기";
			this.btn_open.UseVisualStyleBackColor = true;
			// 
			// btn_next
			// 
			this.btn_next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_next.Location = new System.Drawing.Point(268, 295);
			this.btn_next.Name = "btn_next";
			this.btn_next.Size = new System.Drawing.Size(150, 39);
			this.btn_next.TabIndex = 1;
			this.btn_next.Text = ">>";
			this.btn_next.UseVisualStyleBackColor = true;
			// 
			// btn_past
			// 
			this.btn_past.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btn_past.Location = new System.Drawing.Point(3, 295);
			this.btn_past.Name = "btn_past";
			this.btn_past.Size = new System.Drawing.Size(150, 39);
			this.btn_past.TabIndex = 1;
			this.btn_past.Text = "<<";
			this.btn_past.UseVisualStyleBackColor = true;
			// 
			// Picturebox
			// 
			this.Picturebox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.Picturebox.Location = new System.Drawing.Point(3, 0);
			this.Picturebox.Name = "Picturebox";
			this.Picturebox.Size = new System.Drawing.Size(417, 291);
			this.Picturebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.Picturebox.TabIndex = 0;
			this.Picturebox.TabStop = false;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.btn_stop);
			this.groupBox1.Controls.Add(this.btn_start);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.numericUpDown1);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.comboBox1);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.btn_explorer);
			this.groupBox1.Controls.Add(this.tb_path);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(3, 3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(578, 114);
			this.groupBox1.TabIndex = 2;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "설정";
			// 
			// btn_stop
			// 
			this.btn_stop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_stop.Enabled = false;
			this.btn_stop.Location = new System.Drawing.Point(417, 86);
			this.btn_stop.Name = "btn_stop";
			this.btn_stop.Size = new System.Drawing.Size(75, 23);
			this.btn_stop.TabIndex = 8;
			this.btn_stop.Text = "캡처 중지";
			this.btn_stop.UseVisualStyleBackColor = true;
			// 
			// btn_start
			// 
			this.btn_start.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_start.Location = new System.Drawing.Point(498, 85);
			this.btn_start.Name = "btn_start";
			this.btn_start.Size = new System.Drawing.Size(75, 23);
			this.btn_start.TabIndex = 8;
			this.btn_start.Text = "캡처 시작";
			this.btn_start.UseVisualStyleBackColor = true;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(436, 54);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(130, 12);
			this.label4.TabIndex = 7;
			this.label4.Text = "초 (1 ~ 3600초(1시간))";
			// 
			// numericUpDown1
			// 
			this.numericUpDown1.Location = new System.Drawing.Point(310, 50);
			this.numericUpDown1.Maximum = new decimal(new int[] {
            3600,
            0,
            0,
            0});
			this.numericUpDown1.Name = "numericUpDown1";
			this.numericUpDown1.Size = new System.Drawing.Size(120, 21);
			this.numericUpDown1.TabIndex = 6;
			this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.numericUpDown1.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(220, 54);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(93, 12);
			this.label3.TabIndex = 5;
			this.label3.Text = "화면 저장 주기: ";
			// 
			// comboBox1
			// 
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Location = new System.Drawing.Point(93, 51);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(121, 20);
			this.comboBox1.TabIndex = 4;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(20, 54);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(73, 12);
			this.label2.TabIndex = 3;
			this.label2.Text = "파일 확장자:";
			// 
			// btn_explorer
			// 
			this.btn_explorer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btn_explorer.Location = new System.Drawing.Point(498, 25);
			this.btn_explorer.Name = "btn_explorer";
			this.btn_explorer.Size = new System.Drawing.Size(68, 23);
			this.btn_explorer.TabIndex = 2;
			this.btn_explorer.Text = "탐색";
			this.btn_explorer.UseVisualStyleBackColor = true;
			// 
			// tb_path
			// 
			this.tb_path.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tb_path.Location = new System.Drawing.Point(93, 27);
			this.tb_path.Name = "tb_path";
			this.tb_path.Size = new System.Drawing.Size(399, 21);
			this.tb_path.TabIndex = 1;
			this.tb_path.Text = "C:\\Users\\Public\\Pictures\\Sample Pictures";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(20, 30);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(77, 12);
			this.label1.TabIndex = 0;
			this.label1.Text = "저장할 폴더: ";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(584, 462);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "Form1";
			this.Text = "AutoScreenShot3";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Picturebox)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_explorer;
        private System.Windows.Forms.TextBox tb_path;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_stop;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_del;
        private System.Windows.Forms.Button btn_clb_reverse;
        private System.Windows.Forms.CheckedListBox clb_filelist;
        private System.Windows.Forms.PictureBox Picturebox;
        private System.Windows.Forms.Button btn_open;
        private System.Windows.Forms.Button btn_next;
        private System.Windows.Forms.Button btn_past;
    }
}

