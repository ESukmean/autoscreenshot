﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AutoScreenShot3
{
    public partial class Form1 : Form
    {
        ScreenShot ss;
        public string path = "";
        public Form1(ref ScreenShot rss)
        {
            ss = rss;
            InitializeComponent();

            btn_explorer.Click += btn_explorer_Click;
            clb_filelist.SelectedValueChanged += clb_filelist_SelectedValueChanged;
            btn_clb_reverse.Click += btn_clb_reverse_Click;
            btn_del.Click += btn_del_Click;

			ss.captured += ss_captured;
            btn_open.Click += btn_open_Click;
            btn_next.Click += btn_next_Click;
            btn_past.Click += btn_past_Click;

			btn_start.Click += btn_start_Click;
			btn_stop.Click += btn_stop_Click;
            get_filelist();

			CheckForIllegalCrossThreadCalls = false;
        }

		void ss_captured(string filename)
		{
			clb_filelist.Items.Add(filename);
		}

		void btn_stop_Click(object sender, EventArgs e)
		{
			if (ss.running() == false)
				ss.stop();
		}
		void btn_start_Click(object sender, EventArgs e)
		{
			set_path(tb_path.Text);
			ss.interval = (int)numericUpDown1.Value;

			if (ss.running())
				return;

			ss.start();
		}

		void btn_open_Click(object sender, EventArgs e)
        {
			if (clb_filelist.SelectedItem == null)
				return;

			if (System.IO.File.Exists(path + "\\" + clb_filelist.SelectedItem.ToString()))
			{
				System.Diagnostics.Process.Start(path + "\\" + clb_filelist.SelectedItem.ToString());
				return;
			}

			MessageBox.Show("해당하는 파일이 존재하지 않습니다.");
        }
        void btn_past_Click(object sender, EventArgs e)
        {
            if (clb_filelist.Items.Count < 1) return;

            clb_filelist.SelectedIndex = clb_filelist.SelectedIndex == 0 ? clb_filelist.Items.Count - 1 : --clb_filelist.SelectedIndex;
        }
        void btn_next_Click(object sender, EventArgs e)
        {
            if (clb_filelist.Items.Count < 1) return;

            clb_filelist.SelectedIndex = clb_filelist.SelectedIndex == clb_filelist.Items.Count - 1 ? 0 : ++clb_filelist.SelectedIndex;
        }
        void btn_del_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < clb_filelist.CheckedItems.Count; i++)
            {
                System.IO.File.Delete(tb_path.Text + "\\" + clb_filelist.CheckedItems[i].ToString());
				clb_filelist.Items.Remove(clb_filelist.CheckedItems[i]);
            }
        }
        void btn_clb_reverse_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < clb_filelist.Items.Count; i++)
            {
                if(clb_filelist.GetItemChecked(i))
                    clb_filelist.SetItemChecked(i, false);
                else
                    clb_filelist.SetItemChecked(i, true);
            }
        }

		bool set_path(string path)
		{
			if (path == ss.path)
				return true;

			if (System.IO.Directory.Exists(path) == false)
			{
				try
				{
					System.IO.Directory.CreateDirectory(path);
				}
				catch
				{
					MessageBox.Show("해당 폴더("+path+") 를 만드실 수 없습니다.", "캡처 불가", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return false;
				}
			}
			tb_path.Text = path;
			get_filelist(tb_path.Text, true);
			ss.path = tb_path.Text;
			this.path = path;

			return true;
		}
        void add_filelist(string item)
        {
			if (clb_filelist.Items.IndexOf(item) == -1)
			{
				clb_filelist.Items.Add(item);
			}
			else
			{
				Console.WriteLine(item); 
			}
                
        }
        void get_filelist(string path = "", bool refresh = false)
        {
            if (path == "") path = tb_path.Text;
			if (refresh) clb_filelist.Items.Clear();

			CheckedListBox.ObjectCollection cbi = clb_filelist.Items;

            string[] f = System.IO.Directory.GetFiles(path);
            for (int i = 0; i < f.Length; i++)
            {
                switch (System.IO.Path.GetExtension(f[i]))
                {
                    case ".jpg":
                    case ".png":
                    case ".bmp":
                    case ".gif":
						if (cbi.IndexOf(System.IO.Path.GetFileName(f[i])) > -1)
							cbi.Remove(System.IO.Path.GetFileName(f[i]));

                        add_filelist(System.IO.Path.GetFileName(f[i]));
                        break;

                    default:
                        break;
                }
            }

			for (int i = 0; i < cbi.Count; i++)
			{
				clb_filelist.Items.Remove(cbi[i]);
			}

            this.path = path;
        }
        void clb_filelist_SelectedValueChanged(object sender, EventArgs e)
        {
			if (clb_filelist.SelectedItem == null)
				return;

            if (System.IO.File.Exists(tb_path.Text + "\\" + clb_filelist.SelectedItem.ToString()))
                Picturebox.ImageLocation = tb_path.Text + "\\" + clb_filelist.SelectedItem.ToString();
        }
        void btn_explorer_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog fbd = new FolderBrowserDialog())
            {
                fbd.SelectedPath = System.IO.Directory.Exists(tb_path.Text) ? tb_path.Text : Environment.CurrentDirectory;

                DialogResult dr = fbd.ShowDialog();
                if (dr == System.Windows.Forms.DialogResult.OK)
                {
					if (tb_path.Text != fbd.SelectedPath)
						set_path(fbd.SelectedPath);
                }   
            }
        }      
    }
}
