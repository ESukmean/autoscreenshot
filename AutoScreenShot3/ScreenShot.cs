﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoScreenShot3
{
	public enum FileType
	{
		png, jpg, gif, bmp
	}
	public enum StartRes
	{
		already_start, no_path, success
	}

	public class ScreenShot
	{
		public delegate void Captured(string filename);
		public event Captured captured;

		private string in_path = "";
		private int in_interval = 0;
		private FileType in_filetype = FileType.png;
		private System.Drawing.Imaging.ImageFormat IF;
		private System.Text.StringBuilder sb;
		private int cycle = 0;
		private System.Threading.Thread th = null;

		public ScreenShot(FileType FT = FileType.png, string path = "", int interval = 10)
		{
			this.sb = new StringBuilder(30);
			IF = System.Drawing.Imaging.ImageFormat.Png;
			this.in_filetype = FT;
			this.in_path = path;
			this.in_interval = interval;
		}
	   
		public FileType filetype
		{
			get
			{
				return in_filetype;
			}
			set
			{
				in_filetype = value;
				switch (value)
				{
					case FileType.png:
						if (IF != System.Drawing.Imaging.ImageFormat.Png)
							IF = System.Drawing.Imaging.ImageFormat.Png;

						break;
					case FileType.jpg:
						if (IF != System.Drawing.Imaging.ImageFormat.Jpeg)
							IF = System.Drawing.Imaging.ImageFormat.Jpeg;

						break;
					case FileType.gif:
						if (IF != System.Drawing.Imaging.ImageFormat.Gif)
							IF = System.Drawing.Imaging.ImageFormat.Gif;

						break;
					case FileType.bmp:
						if (IF != System.Drawing.Imaging.ImageFormat.Bmp)
							IF = System.Drawing.Imaging.ImageFormat.Bmp;

						break;
					default:
						return;
				}

				AutoScreenShot3.Properties.Settings.Default.FileType = (int)value;
			}
		}
		public string path
		{
			get
			{
				return in_path;
			}
			set
			{
				if (System.IO.Directory.Exists(value) == false)
					return;

				in_path = value;
				AutoScreenShot3.Properties.Settings.Default.path = value;
			}
		}
		public int interval
		{
			get
			{
				return in_interval;
			}
			set
			{
				if (value < 1 || value > 3600)
					return;

				in_interval = value * 1000;
				AutoScreenShot3.Properties.Settings.Default.interval = value;
			}
		}
		public bool running()
		{
			if (cycle % 2 == 1) return true;

			return false;
		}

		public StartRes start()
		{
			if (cycle % 2 == 1) return StartRes.already_start;
			if (in_path == "") return StartRes.no_path;

			++cycle;
			th = new System.Threading.Thread(capture_proc);
			th.Start();
			return StartRes.success;
		}
		public void stop()
		{
			++cycle;
		}
		private string filename()
		{
			sb.Length = 0;

			sb.Append(System.DateTime.Now.ToLongDateString() + " " + System.DateTime.Now.ToLongTimeString().Replace(':', '-'));
			switch (in_filetype)
			{
				case FileType.jpg:
					sb.Append(".jpg");
					break;
				case FileType.gif:
					sb.Append(".gif");
					break;
				case FileType.bmp:
					sb.Append(".bmp");
					break;
				case FileType.png:
					sb.Append(".png");
					break;
				default:
					break;
			}

			return sb.ToString();
		}
		private bool vaildate()
		{
			try
			{
				if (System.IO.Directory.Exists(in_path) == false)
					System.IO.Directory.CreateDirectory(in_path);
			}
			catch
			{
				return false;
			}

			return true;
		}

		private void capture_proc()
		{
			int check_cycle = cycle;

			System.Drawing.Bitmap bm = new System.Drawing.Bitmap(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width, System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height);
			System.Drawing.Graphics gp = System.Drawing.Graphics.FromImage(bm);
			string fn = "";
			while (check_cycle == cycle && vaildate())
			{
				if (bm.Size.Width != System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width || bm.Size.Height != System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height)
				{
					gp.Dispose();
					bm.Dispose();

					bm = new System.Drawing.Bitmap(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width, System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height);
					gp = System.Drawing.Graphics.FromImage(bm);
				}

				gp.CopyFromScreen(0, 0, 0, 0, bm.Size);
				bm.Save(string.Concat(in_path, "\\", (fn = filename())), IF);
				captured(fn);
				System.Threading.Thread.Sleep(in_interval);
			}
		}
	}
}
